-- MIGRATION SCRIPT
-- Database Type: DBType{type='mysql', urlScheme='jdbc:', dialect='org.hibernate.dialect.MySQLDialect', driverClass='org.mariadb.jdbc.Driver'}
-- App Name: test_repo_fawb
-- Database: Dublin
-- Created At: 30-Sep-2020 11:36:32.UTC
-- Script Version: 1.0
-- -----------------------------------------------------------------
-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: Dublin
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.25-MariaDB-10.2.25+maria~xenial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Address`
--

DROP TABLE IF EXISTS `Address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Address` (
  `AddressId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `ClientId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `AddressType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StateId` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`AddressId`),
  KEY `Address_ClientId` (`ClientId`),
  KEY `Address_StateId` (`StateId`),
  CONSTRAINT `Address_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `Client` (`ClientId`),
  CONSTRAINT `Address_StateId` FOREIGN KEY (`StateId`) REFERENCES `StateMaster` (`StateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Agent`
--

DROP TABLE IF EXISTS `Agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Agent` (
  `AgentId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `AgentNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailId` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `AgentName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MaritalStatus` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nationality` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gender` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MobileNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PhoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SSN` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Occupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PictureUrl` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`AgentId`),
  KEY `Agent_UserId` (`UserId`),
  CONSTRAINT `Agent_UserId` FOREIGN KEY (`UserId`) REFERENCES `User` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AgentTarget`
--

DROP TABLE IF EXISTS `AgentTarget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AgentTarget` (
  `UniqueTargetId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `AgentId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `CurrentMonth` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Target` decimal(18,0) DEFAULT NULL,
  PRIMARY KEY (`UniqueTargetId`),
  KEY `AgentTarget_AgentId` (`AgentId`),
  CONSTRAINT `AgentTarget_AgentId` FOREIGN KEY (`AgentId`) REFERENCES `Agent` (`AgentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Client`
--

DROP TABLE IF EXISTS `Client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Client` (
  `ClientId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `ClientNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailId` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `ClientName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MaritalStatus` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NoOfChildren` int(11) DEFAULT NULL,
  `Nationality` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gender` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MobileNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PhoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SSN` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Qualification` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Company` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OccupationalHazards` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AnnualIncome` decimal(12,2) DEFAULT NULL,
  `Occupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Height` decimal(5,2) DEFAULT NULL,
  `Weight` decimal(5,2) DEFAULT NULL,
  `ChangeInWeight` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ClientId`),
  KEY `Client_UserId` (`UserId`),
  CONSTRAINT `Client_UserId` FOREIGN KEY (`UserId`) REFERENCES `User` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClientAgentPolicyMapping`
--

DROP TABLE IF EXISTS `ClientAgentPolicyMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClientAgentPolicyMapping` (
  `ClientAgentPolicyMappingId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `ClientId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `AgentId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `UniquePolicyId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `SumAssured` decimal(12,2) DEFAULT NULL,
  `PolicyId` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApplicationId` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PremiumToBePaid` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `PremiumType` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PolicyStatusId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WithdrawalAmount` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Equity` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Debt` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoneyMarket` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AbsouteReturn` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `TobaccoIntake` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Term` int(11) DEFAULT NULL,
  `ProcessStatus` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Comments` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProcessInstanceId` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ClientAgentPolicyMappingId`),
  KEY `ClientAgentMapping_ClientId` (`ClientId`),
  KEY `ClientAgentMapping_AgentId` (`AgentId`),
  KEY `ClientAgentMapping_PolicyStatusId` (`PolicyStatusId`),
  KEY `ClientAgentMapping_UniquePolicyId` (`UniquePolicyId`),
  CONSTRAINT `ClientAgentMapping_AgentId` FOREIGN KEY (`AgentId`) REFERENCES `Agent` (`AgentId`),
  CONSTRAINT `ClientAgentMapping_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `Client` (`ClientId`),
  CONSTRAINT `ClientAgentMapping_PolicyStatusId` FOREIGN KEY (`PolicyStatusId`) REFERENCES `StatusList` (`PolicyStatusId`),
  CONSTRAINT `ClientAgentMapping_UniquePolicyId` FOREIGN KEY (`UniquePolicyId`) REFERENCES `MasterPolicy` (`UniquePolicyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MasterPolicy`
--

DROP TABLE IF EXISTS `MasterPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MasterPolicy` (
  `UniquePolicyId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `PlanName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CurrentFundValue` decimal(12,2) DEFAULT NULL,
  `CashValue` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`UniquePolicyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NeedAnalysisForAge`
--

DROP TABLE IF EXISTS `NeedAnalysisForAge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NeedAnalysisForAge` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `AgeRange` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CA` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NeedAnalysisForIncome`
--

DROP TABLE IF EXISTS `NeedAnalysisForIncome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NeedAnalysisForIncome` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `IncomeRange` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CAI` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Nominees`
--

DROP TABLE IF EXISTS `Nominees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Nominees` (
  `NomineeId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `ClientAgentPolicyMappingId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `ClientId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `FirstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Relationship` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EntilementPercentage` int(11) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `MobileNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PhoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StateId` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`NomineeId`),
  KEY `Nominees_ClientAgentPolicyMappingId` (`ClientAgentPolicyMappingId`),
  KEY `Nominees_Client` (`ClientId`),
  CONSTRAINT `Nominees_Client` FOREIGN KEY (`ClientId`) REFERENCES `Client` (`ClientId`),
  CONSTRAINT `Nominees_ClientAgentPolicyMappingId` FOREIGN KEY (`ClientAgentPolicyMappingId`) REFERENCES `ClientAgentPolicyMapping` (`ClientAgentPolicyMappingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PolicyPayment`
--

DROP TABLE IF EXISTS `PolicyPayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PolicyPayment` (
  `PolicyPaymentId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TransactionId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ClientAgentPolicyMappingId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `ClientId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `AgentId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `PremiumPaid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NetCommission` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PaymentDate` datetime DEFAULT NULL,
  `CurrentPremiumDueDate` datetime DEFAULT NULL,
  `NextDueDate` datetime DEFAULT NULL,
  `PaymentMode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`PolicyPaymentId`),
  KEY `PolicyPayment_ClientId` (`ClientId`),
  KEY `PolicyPayment_AgentId` (`AgentId`),
  KEY `PolicyPayment_ClientAgentPolicyMappingId` (`ClientAgentPolicyMappingId`),
  CONSTRAINT `PolicyPayment_AgentId` FOREIGN KEY (`AgentId`) REFERENCES `ClientAgentPolicyMapping` (`AgentId`),
  CONSTRAINT `PolicyPayment_ClientAgentPolicyMappingId` FOREIGN KEY (`ClientAgentPolicyMappingId`) REFERENCES `ClientAgentPolicyMapping` (`ClientAgentPolicyMappingId`),
  CONSTRAINT `PolicyPayment_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `ClientAgentPolicyMapping` (`ClientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PremiumAnalysisForAge`
--

DROP TABLE IF EXISTS `PremiumAnalysisForAge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PremiumAnalysisForAge` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `AgeRange` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CA` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PremiumAnalysisForGender`
--

DROP TABLE IF EXISTS `PremiumAnalysisForGender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PremiumAnalysisForGender` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `Gender` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CG` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PremiumAnalysisForSum`
--

DROP TABLE IF EXISTS `PremiumAnalysisForSum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PremiumAnalysisForSum` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `SumAssured` decimal(12,2) DEFAULT NULL,
  `CS` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PremiumAnalysisForTerm`
--

DROP TABLE IF EXISTS `PremiumAnalysisForTerm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PremiumAnalysisForTerm` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `Term` int(11) DEFAULT NULL,
  `CT` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PremiumAnalysisForTobacco`
--

DROP TABLE IF EXISTS `PremiumAnalysisForTobacco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PremiumAnalysisForTobacco` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `Tobacco` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CTO` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `RoleId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `RoleName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StateMaster`
--

DROP TABLE IF EXISTS `StateMaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StateMaster` (
  `StateId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `StateName` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `StateCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GeoGroup` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`StateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StatusList`
--

DROP TABLE IF EXISTS `StatusList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StatusList` (
  `PolicyStatusId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PolicyStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PolicyStatusId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `UserId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `UserName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserRoleMapping`
--

DROP TABLE IF EXISTS `UserRoleMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserRoleMapping` (
  `UserRoleMappingId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `RoleId` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`UserRoleMappingId`),
  KEY `UserRoleMapping_RoleId` (`RoleId`),
  KEY `UserRoleMapping_UserId` (`UserId`),
  CONSTRAINT `UserRoleMapping_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `Role` (`RoleId`),
  CONSTRAINT `UserRoleMapping_UserId` FOREIGN KEY (`UserId`) REFERENCES `User` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-30 11:36:33
