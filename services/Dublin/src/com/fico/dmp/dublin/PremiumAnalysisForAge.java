/*Copyright (c) 2020-2021 fico.com All Rights Reserved.
 This software is the confidential and proprietary information of fico.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with fico.com*/
package com.fico.dmp.dublin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * PremiumAnalysisForAge generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`PremiumAnalysisForAge`")
public class PremiumAnalysisForAge implements Serializable {

    private String id;
    private String ageRange;
    private Double ca;

    @Id
    @Column(name = "`id`", nullable = false, length = 36)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "`AgeRange`", nullable = true, length = 100)
    public String getAgeRange() {
        return this.ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    @Column(name = "`CA`", nullable = true, scale = 2, precision = 8)
    public Double getCa() {
        return this.ca;
    }

    public void setCa(Double ca) {
        this.ca = ca;
    }





    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PremiumAnalysisForAge)) return false;
        final PremiumAnalysisForAge premiumAnalysisForAge = (PremiumAnalysisForAge) o;
        return Objects.equals(getId(), premiumAnalysisForAge.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}