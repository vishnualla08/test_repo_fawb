/*Copyright (c) 2020-2021 fico.com All Rights Reserved.
 This software is the confidential and proprietary information of fico.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with fico.com*/
package com.fico.dmp.dublin.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.commons.wrapper.StringWrapper;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.manager.ExportedFileManager;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.runtime.security.xss.XssDisable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.fico.dmp.dublin.Address;
import com.fico.dmp.dublin.StateMaster;
import com.fico.dmp.dublin.service.StateMasterService;


/**
 * Controller object for domain model class StateMaster.
 * @see StateMaster
 */
@RestController("Dublin.StateMasterController")
@Api(value = "StateMasterController", description = "Exposes APIs to work with StateMaster resource.")
@RequestMapping("/Dublin/StateMaster")
public class StateMasterController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateMasterController.class);

    @Autowired
	@Qualifier("Dublin.StateMasterService")
	private StateMasterService stateMasterService;

	@Autowired
	private ExportedFileManager exportedFileManager;

	@ApiOperation(value = "Creates a new StateMaster instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public StateMaster createStateMaster(@RequestBody StateMaster stateMaster) {
		LOGGER.debug("Create StateMaster with information: {}" , stateMaster);

		stateMaster = stateMasterService.create(stateMaster);
		LOGGER.debug("Created StateMaster with information: {}" , stateMaster);

	    return stateMaster;
	}

    @ApiOperation(value = "Returns the StateMaster instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public StateMaster getStateMaster(@PathVariable("id") String id) {
        LOGGER.debug("Getting StateMaster with id: {}" , id);

        StateMaster foundStateMaster = stateMasterService.getById(id);
        LOGGER.debug("StateMaster details with id: {}" , foundStateMaster);

        return foundStateMaster;
    }

    @ApiOperation(value = "Updates the StateMaster instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public StateMaster editStateMaster(@PathVariable("id") String id, @RequestBody StateMaster stateMaster) {
        LOGGER.debug("Editing StateMaster with id: {}" , stateMaster.getStateId());

        stateMaster.setStateId(id);
        stateMaster = stateMasterService.update(stateMaster);
        LOGGER.debug("StateMaster details with id: {}" , stateMaster);

        return stateMaster;
    }

    @ApiOperation(value = "Deletes the StateMaster instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteStateMaster(@PathVariable("id") String id) {
        LOGGER.debug("Deleting StateMaster with id: {}" , id);

        StateMaster deletedStateMaster = stateMasterService.delete(id);

        return deletedStateMaster != null;
    }

    /**
     * @deprecated Use {@link #findStateMasters(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of StateMaster instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Page<StateMaster> searchStateMastersByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering StateMasters list by query filter:{}", (Object) queryFilters);
        return stateMasterService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of StateMaster instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<StateMaster> findStateMasters(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering StateMasters list by filter:", query);
        return stateMasterService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of StateMaster instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Page<StateMaster> filterStateMasters(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering StateMasters list by filter", query);
        return stateMasterService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Downloadable exportStateMasters(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return stateMasterService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns a URL to download a file for the data matching the optional query (q) request param and the required fields provided in the Export Options.") 
    @RequestMapping(value = "/export", method = {RequestMethod.POST}, consumes = "application/json")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public StringWrapper exportStateMastersAndGetURL(@RequestBody DataExportOptions exportOptions, Pageable pageable) {
        String exportedFileName = exportOptions.getFileName();
        if(exportedFileName == null || exportedFileName.isEmpty()) {
            exportedFileName = StateMaster.class.getSimpleName();
        }
        exportedFileName += exportOptions.getExportType().getExtension();
        String exportedUrl = exportedFileManager.registerAndGetURL(exportedFileName, outputStream -> stateMasterService.export(exportOptions, pageable, outputStream));
        return new StringWrapper(exportedUrl);
    }

	@ApiOperation(value = "Returns the total count of StateMaster instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	@XssDisable
	public Long countStateMasters( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting StateMasters");
		return stateMasterService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	@XssDisable
	public Page<Map<String, Object>> getStateMasterAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return stateMasterService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/addresses", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the addresses instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Address> findAssociatedAddresses(@PathVariable("id") String id, Pageable pageable) {

        LOGGER.debug("Fetching all associated addresses");
        return stateMasterService.findAssociatedAddresses(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service StateMasterService instance
	 */
	protected void setStateMasterService(StateMasterService service) {
		this.stateMasterService = service;
	}

}