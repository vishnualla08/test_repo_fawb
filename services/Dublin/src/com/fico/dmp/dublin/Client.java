/*Copyright (c) 2020-2021 fico.com All Rights Reserved.
 This software is the confidential and proprietary information of fico.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with fico.com*/
package com.fico.dmp.dublin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Client generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`Client`", indexes = {
            @Index(name = "`Client_UserId`", columnList = "`UserId`")})
public class Client implements Serializable {

    private String clientId;
    private String userId;
    private String clientNumber;
    private String emailId;
    private Date dateOfBirth;
    private String clientName;
    private String maritalStatus;
    private Integer noOfChildren;
    private String nationality;
    private String gender;
    private String mobileNumber;
    private String phoneNumber;
    private String ssn;
    private String address;
    private String qualification;
    private String company;
    private String occupationalHazards;
    private Double annualIncome;
    private String occupation;
    private Float height;
    private Float weight;
    private String changeInWeight;
    private User user;

    @Id
    @Column(name = "`ClientId`", nullable = false, length = 36)
    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Column(name = "`UserId`", nullable = false, length = 36)
    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "`ClientNumber`", nullable = true, length = 20)
    public String getClientNumber() {
        return this.clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    @Column(name = "`EmailId`", nullable = true, length = 100)
    public String getEmailId() {
        return this.emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Column(name = "`DateOfBirth`", nullable = true)
    public Date getDateOfBirth() {
        return this.dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Column(name = "`ClientName`", nullable = true, length = 200)
    public String getClientName() {
        return this.clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @Column(name = "`MaritalStatus`", nullable = true, length = 20)
    public String getMaritalStatus() {
        return this.maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Column(name = "`NoOfChildren`", nullable = true, scale = 0, precision = 10)
    public Integer getNoOfChildren() {
        return this.noOfChildren;
    }

    public void setNoOfChildren(Integer noOfChildren) {
        this.noOfChildren = noOfChildren;
    }

    @Column(name = "`Nationality`", nullable = true, length = 20)
    public String getNationality() {
        return this.nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Column(name = "`Gender`", nullable = true, length = 20)
    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "`MobileNumber`", nullable = true, length = 20)
    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Column(name = "`PhoneNumber`", nullable = true, length = 20)
    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "`SSN`", nullable = true, length = 20)
    public String getSsn() {
        return this.ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    @Column(name = "`Address`", nullable = true, length = 1000)
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "`Qualification`", nullable = true, length = 1000)
    public String getQualification() {
        return this.qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Column(name = "`Company`", nullable = true, length = 1000)
    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Column(name = "`OccupationalHazards`", nullable = true, length = 20)
    public String getOccupationalHazards() {
        return this.occupationalHazards;
    }

    public void setOccupationalHazards(String occupationalHazards) {
        this.occupationalHazards = occupationalHazards;
    }

    @Column(name = "`AnnualIncome`", nullable = true, scale = 2, precision = 12)
    public Double getAnnualIncome() {
        return this.annualIncome;
    }

    public void setAnnualIncome(Double annualIncome) {
        this.annualIncome = annualIncome;
    }

    @Column(name = "`Occupation`", nullable = true, length = 100)
    public String getOccupation() {
        return this.occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Column(name = "`Height`", nullable = true, scale = 2, precision = 5)
    public Float getHeight() {
        return this.height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    @Column(name = "`Weight`", nullable = true, scale = 2, precision = 5)
    public Float getWeight() {
        return this.weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    @Column(name = "`ChangeInWeight`", nullable = true, length = 20)
    public String getChangeInWeight() {
        return this.changeInWeight;
    }

    public void setChangeInWeight(String changeInWeight) {
        this.changeInWeight = changeInWeight;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`UserId`", referencedColumnName = "`UserId`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`Client_UserId`"))
    @Fetch(FetchMode.JOIN)
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        if(user != null) {
            this.userId = user.getUserId();
        }

        this.user = user;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        final Client client = (Client) o;
        return Objects.equals(getClientId(), client.getClientId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClientId());
    }
}