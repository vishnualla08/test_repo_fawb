/*Copyright (c) 2020-2021 fico.com All Rights Reserved.
 This software is the confidential and proprietary information of fico.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with fico.com*/
package com.fico.dmp.dublin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * PolicyPayment generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`PolicyPayment`", indexes = {
            @Index(name = "`PolicyPayment_AgentId`", columnList = "`AgentId`"),
            @Index(name = "`PolicyPayment_ClientAgentPolicyMappingId`", columnList = "`ClientAgentPolicyMappingId`"),
            @Index(name = "`PolicyPayment_ClientId`", columnList = "`ClientId`")})
public class PolicyPayment implements Serializable {

    private String policyPaymentId;
    private String transactionId;
    private String clientAgentPolicyMappingId;
    private String clientId;
    private String agentId;
    private String premiumPaid;
    private String netCommission;
    private LocalDateTime paymentDate;
    private LocalDateTime currentPremiumDueDate;
    private LocalDateTime nextDueDate;
    private String paymentMode;
    private ClientAgentPolicyMapping clientAgentPolicyMappingByClientId;
    private ClientAgentPolicyMapping clientAgentPolicyMappingByClientAgentPolicyMappingId;
    private ClientAgentPolicyMapping clientAgentPolicyMappingByAgentId;

    @Id
    @Column(name = "`PolicyPaymentId`", nullable = false, length = 50)
    public String getPolicyPaymentId() {
        return this.policyPaymentId;
    }

    public void setPolicyPaymentId(String policyPaymentId) {
        this.policyPaymentId = policyPaymentId;
    }

    @Column(name = "`TransactionId`", nullable = true, length = 50)
    public String getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Column(name = "`ClientAgentPolicyMappingId`", nullable = false, length = 36)
    public String getClientAgentPolicyMappingId() {
        return this.clientAgentPolicyMappingId;
    }

    public void setClientAgentPolicyMappingId(String clientAgentPolicyMappingId) {
        this.clientAgentPolicyMappingId = clientAgentPolicyMappingId;
    }

    @Column(name = "`ClientId`", nullable = false, length = 36)
    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Column(name = "`AgentId`", nullable = false, length = 36)
    public String getAgentId() {
        return this.agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    @Column(name = "`PremiumPaid`", nullable = true, length = 50)
    public String getPremiumPaid() {
        return this.premiumPaid;
    }

    public void setPremiumPaid(String premiumPaid) {
        this.premiumPaid = premiumPaid;
    }

    @Column(name = "`NetCommission`", nullable = true, length = 50)
    public String getNetCommission() {
        return this.netCommission;
    }

    public void setNetCommission(String netCommission) {
        this.netCommission = netCommission;
    }

    @Column(name = "`PaymentDate`", nullable = true, columnDefinition="DATETIME")
    public LocalDateTime getPaymentDate() {
        return this.paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Column(name = "`CurrentPremiumDueDate`", nullable = true, columnDefinition="DATETIME")
    public LocalDateTime getCurrentPremiumDueDate() {
        return this.currentPremiumDueDate;
    }

    public void setCurrentPremiumDueDate(LocalDateTime currentPremiumDueDate) {
        this.currentPremiumDueDate = currentPremiumDueDate;
    }

    @Column(name = "`NextDueDate`", nullable = true, columnDefinition="DATETIME")
    public LocalDateTime getNextDueDate() {
        return this.nextDueDate;
    }

    public void setNextDueDate(LocalDateTime nextDueDate) {
        this.nextDueDate = nextDueDate;
    }

    @Column(name = "`PaymentMode`", nullable = true, length = 50)
    public String getPaymentMode() {
        return this.paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`ClientId`", referencedColumnName = "`ClientId`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`PolicyPayment_ClientId`"))
    @Fetch(FetchMode.JOIN)
    public ClientAgentPolicyMapping getClientAgentPolicyMappingByClientId() {
        return this.clientAgentPolicyMappingByClientId;
    }

    public void setClientAgentPolicyMappingByClientId(ClientAgentPolicyMapping clientAgentPolicyMappingByClientId) {
        if(clientAgentPolicyMappingByClientId != null) {
            this.clientId = clientAgentPolicyMappingByClientId.getClientId();
        }

        this.clientAgentPolicyMappingByClientId = clientAgentPolicyMappingByClientId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`ClientAgentPolicyMappingId`", referencedColumnName = "`ClientAgentPolicyMappingId`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`PolicyPayment_ClientAgentPolicyMappingId`"))
    @Fetch(FetchMode.JOIN)
    public ClientAgentPolicyMapping getClientAgentPolicyMappingByClientAgentPolicyMappingId() {
        return this.clientAgentPolicyMappingByClientAgentPolicyMappingId;
    }

    public void setClientAgentPolicyMappingByClientAgentPolicyMappingId(ClientAgentPolicyMapping clientAgentPolicyMappingByClientAgentPolicyMappingId) {
        if(clientAgentPolicyMappingByClientAgentPolicyMappingId != null) {
            this.clientAgentPolicyMappingId = clientAgentPolicyMappingByClientAgentPolicyMappingId.getClientAgentPolicyMappingId();
        }

        this.clientAgentPolicyMappingByClientAgentPolicyMappingId = clientAgentPolicyMappingByClientAgentPolicyMappingId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`AgentId`", referencedColumnName = "`AgentId`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`PolicyPayment_AgentId`"))
    @Fetch(FetchMode.JOIN)
    public ClientAgentPolicyMapping getClientAgentPolicyMappingByAgentId() {
        return this.clientAgentPolicyMappingByAgentId;
    }

    public void setClientAgentPolicyMappingByAgentId(ClientAgentPolicyMapping clientAgentPolicyMappingByAgentId) {
        if(clientAgentPolicyMappingByAgentId != null) {
            this.agentId = clientAgentPolicyMappingByAgentId.getAgentId();
        }

        this.clientAgentPolicyMappingByAgentId = clientAgentPolicyMappingByAgentId;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PolicyPayment)) return false;
        final PolicyPayment policyPayment = (PolicyPayment) o;
        return Objects.equals(getPolicyPaymentId(), policyPayment.getPolicyPaymentId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPolicyPaymentId());
    }
}