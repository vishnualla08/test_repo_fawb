/*Copyright (c) 2020-2021 fico.com All Rights Reserved.
 This software is the confidential and proprietary information of fico.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with fico.com*/
package com.fico.dmp.dublin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.fico.dmp.dublin.PremiumAnalysisForGender;


/**
 * ServiceImpl object for domain model class PremiumAnalysisForGender.
 *
 * @see PremiumAnalysisForGender
 */
@Service("Dublin.PremiumAnalysisForGenderService")
@Validated
public class PremiumAnalysisForGenderServiceImpl implements PremiumAnalysisForGenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PremiumAnalysisForGenderServiceImpl.class);


    @Autowired
    @Qualifier("Dublin.PremiumAnalysisForGenderDao")
    private WMGenericDao<PremiumAnalysisForGender, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<PremiumAnalysisForGender, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "DublinTransactionManager")
    @Override
    public PremiumAnalysisForGender create(PremiumAnalysisForGender premiumAnalysisForGender) {
        LOGGER.debug("Creating a new PremiumAnalysisForGender with information: {}", premiumAnalysisForGender);

        PremiumAnalysisForGender premiumAnalysisForGenderCreated = this.wmGenericDao.create(premiumAnalysisForGender);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(premiumAnalysisForGenderCreated);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public PremiumAnalysisForGender getById(String premiumanalysisforgenderId) {
        LOGGER.debug("Finding PremiumAnalysisForGender by id: {}", premiumanalysisforgenderId);
        return this.wmGenericDao.findById(premiumanalysisforgenderId);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public PremiumAnalysisForGender findById(String premiumanalysisforgenderId) {
        LOGGER.debug("Finding PremiumAnalysisForGender by id: {}", premiumanalysisforgenderId);
        try {
            return this.wmGenericDao.findById(premiumanalysisforgenderId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No PremiumAnalysisForGender found with id: {}", premiumanalysisforgenderId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public List<PremiumAnalysisForGender> findByMultipleIds(List<String> premiumanalysisforgenderIds, boolean orderedReturn) {
        LOGGER.debug("Finding PremiumAnalysisForGenders by ids: {}", premiumanalysisforgenderIds);

        return this.wmGenericDao.findByMultipleIds(premiumanalysisforgenderIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "DublinTransactionManager")
    @Override
    public PremiumAnalysisForGender update(PremiumAnalysisForGender premiumAnalysisForGender) {
        LOGGER.debug("Updating PremiumAnalysisForGender with information: {}", premiumAnalysisForGender);

        this.wmGenericDao.update(premiumAnalysisForGender);
        this.wmGenericDao.refresh(premiumAnalysisForGender);

        return premiumAnalysisForGender;
    }

    @Transactional(value = "DublinTransactionManager")
    @Override
    public PremiumAnalysisForGender delete(String premiumanalysisforgenderId) {
        LOGGER.debug("Deleting PremiumAnalysisForGender with id: {}", premiumanalysisforgenderId);
        PremiumAnalysisForGender deleted = this.wmGenericDao.findById(premiumanalysisforgenderId);
        if (deleted == null) {
            LOGGER.debug("No PremiumAnalysisForGender found with id: {}", premiumanalysisforgenderId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), PremiumAnalysisForGender.class.getSimpleName(), premiumanalysisforgenderId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "DublinTransactionManager")
    @Override
    public void delete(PremiumAnalysisForGender premiumAnalysisForGender) {
        LOGGER.debug("Deleting PremiumAnalysisForGender with {}", premiumAnalysisForGender);
        this.wmGenericDao.delete(premiumAnalysisForGender);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public Page<PremiumAnalysisForGender> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all PremiumAnalysisForGenders");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public Page<PremiumAnalysisForGender> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all PremiumAnalysisForGenders");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service Dublin for table PremiumAnalysisForGender to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service Dublin for table PremiumAnalysisForGender to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}