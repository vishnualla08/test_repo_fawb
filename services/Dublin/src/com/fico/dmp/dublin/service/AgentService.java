/*Copyright (c) 2020-2021 fico.com All Rights Reserved.
 This software is the confidential and proprietary information of fico.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with fico.com*/
package com.fico.dmp.dublin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.fico.dmp.dublin.Agent;
import com.fico.dmp.dublin.AgentTarget;
import com.fico.dmp.dublin.ClientAgentPolicyMapping;

/**
 * Service object for domain model class {@link Agent}.
 */
public interface AgentService {

    /**
     * Creates a new Agent. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Agent if any.
     *
     * @param agent Details of the Agent to be created; value cannot be null.
     * @return The newly created Agent.
     */
    Agent create(@Valid Agent agent);


	/**
     * Returns Agent by given id if exists.
     *
     * @param agentIdInstance The id of the Agent to get; value cannot be null.
     * @return Agent associated with the given agentIdInstance.
	 * @throws EntityNotFoundException If no Agent is found.
     */
    Agent getById(String agentIdInstance);

    /**
     * Find and return the Agent by given id if exists, returns null otherwise.
     *
     * @param agentIdInstance The id of the Agent to get; value cannot be null.
     * @return Agent associated with the given agentIdInstance.
     */
    Agent findById(String agentIdInstance);

	/**
     * Find and return the list of Agents by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param agentIdInstances The id's of the Agent to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return Agents associated with the given agentIdInstances.
     */
    List<Agent> findByMultipleIds(List<String> agentIdInstances, boolean orderedReturn);


    /**
     * Updates the details of an existing Agent. It replaces all fields of the existing Agent with the given agent.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Agent if any.
     *
     * @param agent The details of the Agent to be updated; value cannot be null.
     * @return The updated Agent.
     * @throws EntityNotFoundException if no Agent is found with given input.
     */
    Agent update(@Valid Agent agent);

    /**
     * Deletes an existing Agent with the given id.
     *
     * @param agentIdInstance The id of the Agent to be deleted; value cannot be null.
     * @return The deleted Agent.
     * @throws EntityNotFoundException if no Agent found with the given id.
     */
    Agent delete(String agentIdInstance);

    /**
     * Deletes an existing Agent with the given object.
     *
     * @param agent The instance of the Agent to be deleted; value cannot be null.
     */
    void delete(Agent agent);

    /**
     * Find all Agents matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Agents.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<Agent> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all Agents matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Agents.
     *
     * @see Pageable
     * @see Page
     */
    Page<Agent> findAll(String query, Pageable pageable);

    /**
     * Exports all Agents matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all Agents matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the Agents in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the Agent.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated agentTargets for given Agent id.
     *
     * @param agentId value of agentId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated AgentTarget instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<AgentTarget> findAssociatedAgentTargets(String agentId, Pageable pageable);

    /*
     * Returns the associated clientAgentPolicyMappings for given Agent id.
     *
     * @param agentId value of agentId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated ClientAgentPolicyMapping instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<ClientAgentPolicyMapping> findAssociatedClientAgentPolicyMappings(String agentId, Pageable pageable);

}