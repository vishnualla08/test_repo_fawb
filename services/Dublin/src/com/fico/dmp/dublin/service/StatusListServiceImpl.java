/*Copyright (c) 2020-2021 fico.com All Rights Reserved.
 This software is the confidential and proprietary information of fico.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with fico.com*/
package com.fico.dmp.dublin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.fico.dmp.dublin.ClientAgentPolicyMapping;
import com.fico.dmp.dublin.StatusList;


/**
 * ServiceImpl object for domain model class StatusList.
 *
 * @see StatusList
 */
@Service("Dublin.StatusListService")
@Validated
public class StatusListServiceImpl implements StatusListService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusListServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("Dublin.ClientAgentPolicyMappingService")
    private ClientAgentPolicyMappingService clientAgentPolicyMappingService;

    @Autowired
    @Qualifier("Dublin.StatusListDao")
    private WMGenericDao<StatusList, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<StatusList, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "DublinTransactionManager")
    @Override
    public StatusList create(StatusList statusList) {
        LOGGER.debug("Creating a new StatusList with information: {}", statusList);

        StatusList statusListCreated = this.wmGenericDao.create(statusList);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(statusListCreated);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public StatusList getById(String statuslistId) {
        LOGGER.debug("Finding StatusList by id: {}", statuslistId);
        return this.wmGenericDao.findById(statuslistId);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public StatusList findById(String statuslistId) {
        LOGGER.debug("Finding StatusList by id: {}", statuslistId);
        try {
            return this.wmGenericDao.findById(statuslistId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No StatusList found with id: {}", statuslistId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public List<StatusList> findByMultipleIds(List<String> statuslistIds, boolean orderedReturn) {
        LOGGER.debug("Finding StatusLists by ids: {}", statuslistIds);

        return this.wmGenericDao.findByMultipleIds(statuslistIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "DublinTransactionManager")
    @Override
    public StatusList update(StatusList statusList) {
        LOGGER.debug("Updating StatusList with information: {}", statusList);

        this.wmGenericDao.update(statusList);
        this.wmGenericDao.refresh(statusList);

        return statusList;
    }

    @Transactional(value = "DublinTransactionManager")
    @Override
    public StatusList delete(String statuslistId) {
        LOGGER.debug("Deleting StatusList with id: {}", statuslistId);
        StatusList deleted = this.wmGenericDao.findById(statuslistId);
        if (deleted == null) {
            LOGGER.debug("No StatusList found with id: {}", statuslistId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), StatusList.class.getSimpleName(), statuslistId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "DublinTransactionManager")
    @Override
    public void delete(StatusList statusList) {
        LOGGER.debug("Deleting StatusList with {}", statusList);
        this.wmGenericDao.delete(statusList);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public Page<StatusList> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all StatusLists");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public Page<StatusList> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all StatusLists");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service Dublin for table StatusList to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service Dublin for table StatusList to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "DublinTransactionManager")
    @Override
    public Page<ClientAgentPolicyMapping> findAssociatedClientAgentPolicyMappings(String policyStatusId, Pageable pageable) {
        LOGGER.debug("Fetching all associated clientAgentPolicyMappings");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("statusList.policyStatusId = '" + policyStatusId + "'");

        return clientAgentPolicyMappingService.findAll(queryBuilder.toString(), pageable);
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ClientAgentPolicyMappingService instance
     */
    protected void setClientAgentPolicyMappingService(ClientAgentPolicyMappingService service) {
        this.clientAgentPolicyMappingService = service;
    }

}